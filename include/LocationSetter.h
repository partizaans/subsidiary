#ifndef SUBSIDIARY_LOCATIONSETTER_H
#define SUBSIDIARY_LOCATIONSETTER_H

enum LocationSetterState {
    IDLE, SET_X, SET_Y
};

class LocationSetter {
public:

    static void setup();

    static void switchToNextState();

    static LocationSetterState state;

protected:
    static float x, y;

    static void pollForKeys();
};

#endif //SUBSIDIARY_LOCATIONSETTER_H
