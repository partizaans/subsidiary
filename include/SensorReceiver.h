#ifndef SUBSIDIARY_SENSORRECEIVER_H
#define SUBSIDIARY_SENSORRECEIVER_H

#include "Arduino.h"

const int SOUND_SENSOR_PIN = PIND4;

class SensorReceiver {
public:
    static void setup();

    bool getSensorData();
};

#endif //SUBSIDIARY_SENSORRECEIVER_H
