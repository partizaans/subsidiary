#include <Arduino.h>
#include <Wire.h>

struct Coordinate {
    int x;
    int y;
};

Coordinate coordinate = {0, 0};
const int X_Pin = A2;
const int Y_Pin = A3;

const int SOUND_PIN = A0;
int soundDetected = 0;


void requestEvents() {
      Serial.println("hi");
    if (soundDetected != 0)
        Wire.write(1);
    else
    {
         Wire.write(0);
        /* code */
    }
    
}

int calculateAddress(Coordinate c) {
    return ((c.x + c.y) * (c.x + c.y + 1)) / 2 + c.y;
}


Coordinate getCoordinate() {
    int x = analogRead(X_Pin) * 10 / 1024;
    int y = analogRead(Y_Pin) * 10 / 1024;
    return Coordinate{x, y};
}


void setup() {
    pinMode(SOUND_PIN, INPUT);
    pinMode(X_Pin, INPUT);
    pinMode(Y_Pin, INPUT);
    Serial.begin(9600);
    Serial.println("Setup with: " + String(coordinate.x) + ", " + String(coordinate.y));
    Wire.begin(calculateAddress(coordinate));
    Wire.onRequest(requestEvents);
    delay(200);

}

void loop() {
    Coordinate newCoordinate = getCoordinate();
    if (newCoordinate.x != coordinate.x || newCoordinate.y != coordinate.y) {
        coordinate = newCoordinate;
        Serial.println("Got coordinate: " + String(newCoordinate.x) + ", " + String(newCoordinate.y));
        Serial.println("Beginning with the address: " + String(calculateAddress(coordinate)));
        Wire.begin(calculateAddress(coordinate));
        Wire.onRequest(requestEvents);
    }
    soundDetected = digitalRead(SOUND_PIN);
    delay(100);
}


